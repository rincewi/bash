#!/bin/bash

set -e

error='(^[45][0-9]{2})'                                                                                                                                                                 file=./urls.txt

function alive {
  while read line; do
  result=$(curl -Is $line | head -n 1 | cut -d$' ' -f2)
  echo "$result"
  if [[ $result =~ $error ]]; then
    break
  fi
  done < $file
}

if [[ -f "$file" ]]; then
  alive
  else
  echo "file not found"
fi
